#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <Windows.h>
#include <vector>

#include "Game_object.h"
#include "Map.h"
#include "Pokemon.h"
#include "Battle.h"
#include "xBattle.h"

class Battle;
class Map;
class Game_object;
class NPC;

class Plot {
	friend Game;

	bool visited_prof;
	bool visited_queen;
	bool fought_pokemon;
	bool fought_boss;
	bool left_house;

public:
	Plot() {
		visited_prof = false;
		visited_queen = false;
		fought_pokemon = false;
		fought_boss = false;
		left_house = false;
	}
};

class Game {
	int cnt = 0;
	int ran_turns = 10;
	bool is_running;
	bool fight;
	bool ran;
	bool quited;
	bool ended;	
	bool escape_pressed;
	Plot plot;

	SDL_Texture* nic;

	/********************/
	/*		MENU		*/
	/********************/
	SDL_Texture* game_tex;
	SDL_Texture* picked_game;
	SDL_Texture* manual;
	SDL_Texture* picked_manual;
	SDL_Texture* credits;
	SDL_Texture* picked_credits;
	SDL_Texture* quit;
	SDL_Texture* picked_quit;
	SDL_Texture* reset;
	SDL_Texture* picked_reset;

	/********************/
	/*		Talking		*/
	/********************/
	SDL_Surface* surfaceMessage1;
	SDL_Surface* surfaceMessage2;
	SDL_Texture* Message1;
	SDL_Texture* Message2;
//	TTF_Font* Sans;

	SDL_Window* window;
	Game_object* player;
	Map* map;
	std::vector<WildPokemon> vec_poke;
	std::vector<NPC> vec_npc;

public:
	Game() = default;
	~Game() = default;

	int menu();
	void init_window(int xpos, int ypos, int width, int height, bool full_screen);
	void init_game(const std::vector<WildPokemon> temp_p);
	void game_handle();
	void update();
	void render();
	void clear();
	bool running();
	bool has_quited();
	bool has_pressed_esc();
	void neg_esc();
	void neg_finished();
	void rerun();
	bool collision();
	void change_map();
	bool met_poke();
	bool met_NPC();
	bool finished_game();
	void talk_to();
	Pokemon get_poke_from_map();
	NPC get_NPC_from_map();
	void get_pokes_from_map(std::vector<Pokemon>& vec);
	void delete_wild_poke();
	void delete_npc(const std::string& xname);
	void walka();
	void game_end();
	void give_player_poke(const std::vector<Pokemon>& vec_poke);
	void change_poke_order();
	void display_end_scene();
	
	void render_manual();
	void render_creadits();

	static SDL_Renderer* renderer;
};