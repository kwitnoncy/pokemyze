#pragma once
#include <string>
#include <array>
#include <vector>
#include <Windows.h>
#include <iostream>
#include <SDL_image.h>

#include "Skill.h"

class Battle;
class Game;
class xBattle;

class Pokemon {
	friend Battle;
	friend xBattle;
	friend Game;

protected:
	SDL_Texture* texture;
	std::string name;
	int type;
	int health_points;
	int experience_points;
	int base_attack;
	int base_defence;
	int special_attack;
	int special_defence;
	int level;
	int speed;
	int evolution;
	int move_pick;
	int id;
	int base_health_points;
	bool wild = false;

public:
	std::array<Skill, 4> move;
	Pokemon(std::string& name, int type, int health_points, int experience_points, int base_attack,
		int base_defence, int special_attack, int special_defence, int level, int speed, int evolution,
		int move_pick, int id) : name(name), type(type), health_points(health_points), experience_points(experience_points), base_attack(
			base_attack), base_defence(base_defence), special_attack(special_attack), special_defence(special_defence),
		level(level), speed(speed), evolution(evolution), move_pick(move_pick), id(id) {
		base_health_points = health_points;
	};
	Pokemon();

	bool has_health_points();
	int get_health_points();
	int get_max_health_points();
	void print_HP();
	void heal_poke(int x);
	std::string get_name();
	void take_dmg(const int& x);

	bool is_texture();

	void load_teture(const char* filename);

	virtual void setCharmander();
	virtual void setBulbazaur();
	virtual void setSquirtle();
	virtual void setPidgey();
	virtual void setPikachu();
	virtual void setMew();
};


class WildPokemon : Pokemon {
	friend Battle;
	friend Game;

	int pos_X;
	int pos_Y;
	int map_pos_X;
	int map_pos_Y;

public:
	WildPokemon();
	WildPokemon(int new_pos_X, int new_pos_Y, int new_map_pos_X, int new_map_pos_Y);

	void print();

	void setCharmander() override;
	void setBulbazaur() override;
	void setSquirtle() override;
	void setPidgey() override;
	void setPikachu() override;
	void setMew() override;
};
