#include <iostream>
#include <SDL.h>

#include "Item.h"
#include "Texture_manager.h"

Item::Item(std::string xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, int xvalue, Popularity xpop) {
	name = xname;
	xpos = xxpos;
	ypos = xypos;
	map_xpos = xmap_xpos;
	map_ypos = xmap_ypos;
	value = xvalue;
	popularity = xpop;
	type = Error_type;
	picked_up = false;
	ignored = false;

	std::string xpath = "assets/Items/" + xname + ".png";
	const char* path = xpath.c_str();
	tex = Tex_manager::load_texture(path);
}

std::string Item::get_name() {
	return name;
}

Potion::Potion(std::string xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, int xvalue, Popularity xpop) : Item(xname, xxpos, xypos, xmap_xpos, xmap_ypos, xvalue, xpop) {
	type = Type::Potion_type;
}