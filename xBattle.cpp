#include "xBattle.h"

void xBattle::pick_skill_player(Pokemon& p1) {
	bool running = true, first = true;
	std::array<std::array<std::string, 2>, 2> tablica{ {
		{ "1. skill", "2. skill" },
		{ "3. skill", "4. skill" },
	} };
	int posx = 0, posy = 0, sol;

	while (running) {
		SDL_Event* event_skill = new SDL_Event();
		SDL_WaitEvent(event_skill);
		SDL_PollEvent(event_skill);

		switch (event_skill->type) {
			case SDL_KEYDOWN:
				if (event_skill->key.keysym.sym == SDLK_LEFT) {
					posx--;

					if (posx == -1)
						posx = 0;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_RIGHT) {
					posx++;
					if (posx == 2)
						posx = 1;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_UP) {
					posy--;
					if (posy == -1)
						posy = 0;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_DOWN) {
					posy++;
					if (posy == 3)
						posy = 2;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_RETURN or event_skill->key.keysym.sym == SDLK_KP_ENTER) {
					running = false;
				}
		}

		if (posx == 0 && posy == 0)
			sol = 1;
		else if (posx == 0 && posy == 1)
			sol = 3;
		else if (posx == 1 && posy == 0)
			sol = 2;
		else if (posx == 1 && posy == 1)
			sol = 4;

		if (posy == 2)
			sol = 5;

		print_fight_skills(p1, sol);
	}

	p1.move_pick = sol - 1;
}

void xBattle::pick_skill_enemy(Pokemon& p2, const Pokemon& p1){
	float max_dmg = 0;
	int max_move_pick = 0;
	for (int i = 0; i < 4; i++) {
		p2.move_pick = i;
		if (max_dmg < dmg_possible_val(p2, p1))
			max_move_pick = i;
	}

	p2.move_pick = max_move_pick;
}

void xBattle::give_damage(Pokemon& a, Pokemon& d) {
	float dmg = 0.0;
	float MOD = get_eff(a, d) * get_STAB(a) * 2;
	float ad = att_def(a, d);
	dmg = (((((2 * a.level) / 5) + 2 * a.move[a.move_pick].damage * ad) / 50) + 2) * MOD;

	d.take_dmg(int(dmg));
}

float xBattle::att_def(const Pokemon& a, const Pokemon& d) {
	if (a.move[a.move_pick].SPECIAL) {
		return	float(a.special_attack) / float(d.special_defence);
	}
	else {
		return float(a.base_attack) / float(d.base_defence);
	}
}

float xBattle::get_eff(const Pokemon& a, const Pokemon& d) {
	return type_relations[d.type - 1][a.type - 1];
}

float xBattle::get_STAB(const Pokemon& a) {
	if (a.type == a.move[a.move_pick].type)
		return 2.5f;
	else
		return 1.0f;
}

float xBattle::dmg_possible_val(const Pokemon& a, const Pokemon& d) {
	float dmg = 0.0;
	float MOD = get_eff(a, d) * get_STAB(a) * 2;
	dmg = (((((2 * a.level) / 5) + 2 * a.move[a.move_pick].damage * att_def(a, d)) / 50) + 2) * MOD;

	return dmg;
}

bool xBattle::fight(Pokemon& p1, Pokemon& p2) {
	bool fighting = true;

	while (fighting) {
		pick_skill_player(p1);
		pick_skill_enemy(p2, p1);

		if (p1.move_pick < 3) {
			run = true;
			return false;
		}

		if (1) {
			give_damage(p1, p2);
			if (p2.health_points <= 0)
				return true;

			give_damage(p2, p1);
			if (p1.health_points <= 0)
				return false;

		}
		else {
			give_damage(p2, p1);
			if (p1.health_points <= 0)
				return false;

			give_damage(p1, p2);
			if (p2.health_points <= 0)
				return false;
		}
	}
	
	return true;
}

bool xBattle::fight(Player* pla, const Pokemon& p22) {
	bool fighting = true, killed = false;
	Pokemon p1 = pla->pokemons[0];
	Pokemon p2(p22);
	int counter = 1, present_id = 0;

	while (counter < pla->pokemons.size()) {
		if (killed) {
			if (pla->pokemons[counter].get_health_points() > 0) {
				p1 = pla->pokemons[counter];
				killed = false;
				fighting = true;
			}
			else {
				killed = true;
				fighting = false;
			}

			counter++;
			present_id++;
		}
		
		while (fighting) {
			print_fight(p1, p2);												//
			print_fight_pokes(p1, p2);											//	Wyswietlenie itp
			print_fight_hud(p1, p2);											//

			pick_skill_player(p1);												//	wybranie umiejetnosci ataku gracza
			pick_skill_enemy(p2, p1);											//	wybranie umiejetnosci ataku przeciwnika

			if (p1.move_pick == 4) {											//
				std::cout << "udana ucieczka\n";								//
				run = true;														//
				pla->pokemons[present_id] = p1;									//	Obsluga ucieczki
				return false;													//
			}																	//
			
			give_damage(p1, p2);												//	Zadanie obrazen przeciwnikowi

			if (p2.health_points <= 0) {										//
				std::cout << "wygrywa moj pokemon: " << p1.get_name()<<"\n";	//
				pla->pokemons[present_id] = p1;									//	Obsluga zgonu przeciwnika
				return true;													//
			}																	//

			give_damage(p2, p1);												//	Zadanie obrazen pokemonowi gracza

			if (p1.health_points <= 0) {										//
				std::cout << "Ginie moj pokemon: " << p1.name << "\n";			//
				killed = true;													//	Obsluga zgonu pokemona gracza
				pla->pokemons[present_id] = p1;									//
				break;															//
			}
		}
	}

	if (pla->pokemons.size() == 0)
		no_poke = true;

	return false;
}

bool xBattle::get_run() {
	return run;
}

bool xBattle::get_no_poke() {
	return no_poke;
}

void xBattle::print_fight(const Pokemon& p1, const Pokemon& p2) {
	/*
	int type = 0;

	SDL_Rect dest, src;
	SDL_Rect p1_src;
	SDL_Rect p2_src;

	src.x = src.y = p1_src.x = p1_src.y = p2_src.x = p2_src.y = 0;
	src.h = src.w = 64 * 2;
	p1_src.h = p1_src.w = p2_src.h = p2_src.w = 64;
	dest.h = dest.w = 64 * 2;
	dest.x = dest.y = 0;
	
	auto cos = Tex_manager::load_texture("assets/bulba_back.png");
	*/
	SDL_RenderClear(Game::renderer);
}

void xBattle::print_fight_skills(const Pokemon& p1, int move) {
	int skill_couter = 0;
	SDL_Rect s_dest, s_src;

	SDL_Texture* skill1;
	SDL_Texture* skill2;
	SDL_Texture* skill3;
	SDL_Texture* skill4;
	SDL_Texture* picked_skill1;
	SDL_Texture* picked_skill2;
	SDL_Texture* picked_skill3;
	SDL_Texture* picked_skill4;
	SDL_Texture* run1 = Tex_manager::load_texture("assets/skills/RUN.png");

	skill1 = skill2 = skill3 = skill4 = picked_skill1 = picked_skill2 = picked_skill3 = picked_skill4 = Tex_manager::load_texture("assets/charmander_skills/ember.png");

	if (p1.name == "Charmander") {
		skill1 = Tex_manager::load_texture("assets/charmander_skills/ember.png");
		skill2 = Tex_manager::load_texture("assets/charmander_skills/scratch.png");
		skill3 = Tex_manager::load_texture("assets/charmander_skills/tackle.png");
		skill4 = Tex_manager::load_texture("assets/charmander_skills/quickattack.png");
		picked_skill1 = Tex_manager::load_texture("assets/charmander_skills/pickedember.png");
		picked_skill2 = Tex_manager::load_texture("assets/charmander_skills/pickedscratch.png");
		picked_skill3 = Tex_manager::load_texture("assets/charmander_skills/pickedtackle.png");
		picked_skill4 = Tex_manager::load_texture("assets/charmander_skills/pickedquickattack.png");
	}
	else if (p1.name == "Bulbasaur") {
		skill1 = Tex_manager::load_texture("assets/bulba_skills/vine_whip.png");
		skill2 = Tex_manager::load_texture("assets/bulba_skills/scratch.png");
		skill3 = Tex_manager::load_texture("assets/bulba_skills/tackle.png");
		skill4 = Tex_manager::load_texture("assets/bulba_skills/quick_attack.png");
		picked_skill1 = Tex_manager::load_texture("assets/bulba_skills/picked_vine_whip.png");
		picked_skill2 = Tex_manager::load_texture("assets/bulba_skills/picked_scratch.png");
		picked_skill3 = Tex_manager::load_texture("assets/bulba_skills/picked_tackle.png");
		picked_skill4 = Tex_manager::load_texture("assets/bulba_skills/picked_quick_attack.png");
	}
	else if (p1.name == "Mew") {}
	else if (p1.name == "Pidgey") {
		skill1 = Tex_manager::load_texture("assets/pidgey_skills/Gust.png");
		skill2 = Tex_manager::load_texture("assets/pidgey_skills/quickattack.png");
		skill3 = Tex_manager::load_texture("assets/pidgey_skills/Windattack.png");
		skill4 = Tex_manager::load_texture("assets/pidgey_skills/Razorwind.png");
		picked_skill1 = Tex_manager::load_texture("assets/pidgey_skills/pickedGust.png");
		picked_skill2 = Tex_manager::load_texture("assets/pidgey_skills/pickedquickattack.png");
		picked_skill3 = Tex_manager::load_texture("assets/pidgey_skills/pickedWindattack.png");
		picked_skill4 = Tex_manager::load_texture("assets/pidgey_skills/pickedRazorwind.png");
	}
	else if (p1.name == "Squirtle") {
		skill1 = Tex_manager::load_texture("assets/squirtle_skills/tackle.png");
		skill2 = Tex_manager::load_texture("assets/squirtle_skills/Bubble.png");
		skill3 = Tex_manager::load_texture("assets/squirtle_skills/Watergun.png");
		skill4 = Tex_manager::load_texture("assets/squirtle_skills/Bite.png");
		picked_skill1 = Tex_manager::load_texture("assets/squirtle_skills/pickedtackle.png");
		picked_skill2 = Tex_manager::load_texture("assets/squirtle_skills/pickedBubble.png");
		picked_skill3 = Tex_manager::load_texture("assets/squirtle_skills/pickedWatergun.png");
		picked_skill4 = Tex_manager::load_texture("assets/squirtle_skills/pickedBite.png");
	}
	else if (p1.name == "Pikachu") {
		skill1 = Tex_manager::load_texture("assets/pika_skills/Thundershock.png");
		skill2 = Tex_manager::load_texture("assets/pika_skills/quickattack.png");
		skill3 = Tex_manager::load_texture("assets/pika_skills/Thunderbolt.png");
		skill4 = Tex_manager::load_texture("assets/pika_skills/Thunder.png");
		picked_skill1 = Tex_manager::load_texture("assets/pika_skills/pickedThundershock.png");
		picked_skill2 = Tex_manager::load_texture("assets/pika_skills/pickedquickattack.png");
		picked_skill3 = Tex_manager::load_texture("assets/pika_skills/pickedThunderbolt.png");
		picked_skill4 = Tex_manager::load_texture("assets/pika_skills/pickedThunder.png");
	}
	else {
		std::cout << "blad w nazwie posiadanych pokemonow\n";
	}
	
	if (move == 1 || move == 0)
		skill1 = picked_skill1;
	if (move == 2)
		skill2 = picked_skill2;
	if (move == 3)
		skill3 = picked_skill3;
	if (move == 4)
		skill4 = picked_skill4;
	if (move == 5)
		run1 = Tex_manager::load_texture("assets/skills/pick_RUN.png");

	s_src.x = s_src.y = 0;
	s_src.h = 64;
	s_src.w = 256;
	s_dest.h = 64;
	s_dest.w = 256;

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			s_dest.x = j * 64;
			s_dest.y = i * 64;

			if (i == 7 && j == 1)
				Tex_manager::Draw(skill1, s_src, s_dest);
			else if (i == 7 && j == 5)
				Tex_manager::Draw(skill2, s_src, s_dest);
			else if (i == 8 && j == 1)
				Tex_manager::Draw(skill3, s_src, s_dest);
			else if (i == 8 && j == 5)
				Tex_manager::Draw(skill4, s_src, s_dest);
			else if (i == 9 && j == 3)
				Tex_manager::Draw(run1, s_src, s_dest);
		}
	}

	SDL_RenderPresent(Game::renderer);

	SDL_DestroyTexture(skill1);
	SDL_DestroyTexture(skill2);
	SDL_DestroyTexture(skill3);
	SDL_DestroyTexture(skill4);
	SDL_DestroyTexture(picked_skill1);
	SDL_DestroyTexture(picked_skill2);
	SDL_DestroyTexture(picked_skill3);
	SDL_DestroyTexture(picked_skill4);
	SDL_DestroyTexture(run1);
}

void xBattle::print_fight_hud(const Pokemon& p1, const Pokemon& p2) {
	SDL_Rect dest, src, src2;
	SDL_Texture* bar_p = Tex_manager::load_texture("assets/HP_ba.png");
	SDL_Texture* name_bar_p1 = Tex_manager::load_texture("nic.png");
	SDL_Texture* name_bar_p2 = Tex_manager::load_texture("nic.png");

	float pro1, pro2;
	int w1, w2;

	if (p1.name == "Charmander")
		name_bar_p1 = Tex_manager::load_texture("assets/charmander_name.png");
	else if (p1.name == "Bulbasaur")
		name_bar_p1 = Tex_manager::load_texture("assets/bulbasaur_name.png");
	else if (p1.name == "Mew")
		name_bar_p1 = Tex_manager::load_texture("assets/mew_name.png");
	else if (p1.name == "Squirtle")
		name_bar_p1 = Tex_manager::load_texture("assets/squirtle_name.png");
	else if (p1.name == "Pidgey")
		name_bar_p1 = Tex_manager::load_texture("assets/pidgey_name.png");
	else if (p1.name == "Pikachu")
		name_bar_p1 = Tex_manager::load_texture("assets/pikachu_name.png");
	else
		std::cout << "Blad wczytywania tekstury nazwy pokemona gracza\n";


	if (p2.name == "Charmander")
		name_bar_p2 = Tex_manager::load_texture("assets/charmander_name.png");
	else if (p2.name == "Bulbasaur")
		name_bar_p2 = Tex_manager::load_texture("assets/bulbasaur_name.png");
	else if (p2.name == "Mew")
		name_bar_p2 = Tex_manager::load_texture("assets/mew_name.png");
	else if (p2.name == "Squirtle")
		name_bar_p2 = Tex_manager::load_texture("assets/squirtle_name.png");
	else if (p2.name == "Pidgey")
		name_bar_p2 = Tex_manager::load_texture("assets/pidgey_name.png");
	else
		std::cout << "Blad wczytywania tekstury nazwy pokemona gracza\n";


	SDL_LockTexture(bar_p, NULL, 0, 0);
	SDL_LockTexture(name_bar_p1, NULL, 0, 0);
	SDL_LockTexture(name_bar_p2, NULL, 0, 0);

	pro1 = float(p1.health_points) / float(p1.base_health_points);
	pro2 = float(p2.health_points) / float(p2.base_health_points);
	w1 = 256 * pro1;
	w2 = 256 * pro2;

	src.x = 0;
	src.y = 0;
	src.w = 256;
	src.h = 32;

	src2.x = 0;
	src2.y = 0;
	src2.w = 256;
	src2.h = 64;

	dest.x = 65;
	dest.y = 128;
	dest.w = w2;
	dest.h = 32;
	Tex_manager::Draw(bar_p, src, dest);

	dest.x = 65;
	dest.y = 64;
	dest.w = 256;
	dest.h = 64;
	Tex_manager::Draw(name_bar_p2, src2, dest);

	dest.x = 321;
	dest.y = 385;
	dest.w = w1;
	dest.h = 32;
	Tex_manager::Draw(bar_p, src, dest);

	dest.x = 321;
	dest.y = 321;
	dest.w = 256;
	dest.h = 64;
	Tex_manager::Draw(name_bar_p1, src2, dest);

	SDL_RenderPresent(Game::renderer);

	
	SDL_DestroyTexture(name_bar_p1);
	SDL_DestroyTexture(name_bar_p2);
	SDL_DestroyTexture(bar_p);
}

void xBattle::print_fight_pokes(const Pokemon& p1, const Pokemon& p2) {
	SDL_Texture* poke1 = Tex_manager::load_texture("assets/siema.png");
	SDL_Texture* poke2 = Tex_manager::load_texture("assets/siema.png");
	SDL_Rect rect_poke, dest1, dest2;

	if (p1.name == "Charmander")
		poke1 = Tex_manager::load_texture("assets/charmander_back.png");
	else if (p1.name == "Bulbasaur")
		poke1 = Tex_manager::load_texture("assets/bulba_back.png");
	else if (p1.name == "Pidgey")
		poke1 = Tex_manager::load_texture("assets/pidgey_back.png");
	else if (p1.name == "Mew")
		poke1 = Tex_manager::load_texture("assets/mew_back.png");
	else if (p1.name == "Squirtle")
		poke1 = Tex_manager::load_texture("assets/squirtle_back.png");
	else if (p1.name == "Pikachu")
		poke1 = Tex_manager::load_texture("assets/pika_back.png");
	else
		std::cout << "dupa\n";

	if (p2.name == "Charmander")
		poke2 = Tex_manager::load_texture("assets/charmander_front.png");
	else if (p2.name == "Bulbasaur")
		poke2 = Tex_manager::load_texture("assets/bulba_front.png");
	else if (p2.name == "Pidgey")
		poke2 = Tex_manager::load_texture("assets/pidgey_front.png");
	else if (p2.name == "Mew")
		poke2 = Tex_manager::load_texture("assets/mew_front.png");
	else if (p2.name == "Squirtle")
		poke2 = Tex_manager::load_texture("assets/squirtle_front.png");
	else if (p2.name == "Pikachu")
		poke2 = Tex_manager::load_texture("assets/pika_front.png");
	else
		std::cout << "dupa\n";
	
	SDL_LockTexture(poke1, 0, 0, 0);
	SDL_LockTexture(poke2, 0, 0, 0);

	rect_poke.x = rect_poke.y = 0;
	rect_poke.w = rect_poke.h = 128;

	dest1.w = dest1.h = dest2.w = dest2.h = 128;

	dest2.y = 65;
	dest2.x = 384;

	dest1.y = 320;
	dest1.x = 129;

	Tex_manager::Draw(poke1, rect_poke, dest1);
	Tex_manager::Draw(poke2, rect_poke, dest2);
	SDL_RenderPresent(Game::renderer);
}