#pragma once

#include <string>
#include <Windows.h>

#include "Pokemon.h"

class Battle;
class xBattle;
class Pokemon;

class Skill {
	friend Battle;
	friend xBattle;
	friend Pokemon;

	std::string name;
	int type;
	float damage;
	bool target;
	bool SPECIAL;
	SDL_Texture* skil_tex;
	SDL_Texture* pick_skil_tex;

public:
	Skill(std::string new_name, int new_type, float new_damage, bool new_taget, bool new_SPECIAL) {
		name = new_name;
		type = new_type;
		damage = new_damage;
		target = new_taget;
		SPECIAL = new_SPECIAL;
		skil_tex = nullptr;
		pick_skil_tex = nullptr;
	}
	Skill() {
		name = "";
		type = 0;
		damage = 0.0;
		target = true;
		SPECIAL = false;
	}
	bool is_skin() {
		if (skil_tex == nullptr)
			return false;
		else
			return true;
	}
};
