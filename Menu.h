#pragma once

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <string>

class Main_menu {
	bool open;
	int option;
	SDL_Window* window;

	SDL_Texture* game;
	SDL_Texture* picked_game;
	SDL_Texture* manual;
	SDL_Texture* picked_manual;
	SDL_Texture* credits;
	SDL_Texture* picked_credits;
	SDL_Texture* quit;
	SDL_Texture* picked_quit;

public:
	Main_menu() = default;

	void init(const char* title, int xpos, int ypos, int width, int height, bool full_screen);
	void draw(SDL_Texture * tex, SDL_Rect src, SDL_Rect dest);
	void draw_menu();
	void clear();
	void quit_menu();

	int pick_option();
	
	bool is_open();

	SDL_Texture* load_texture(const char* file_name);

	static SDL_Renderer* menu_renderer;
};