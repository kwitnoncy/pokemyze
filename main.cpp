#include <iostream>
#include <array>
#include <vector>
#include <string>
#include <conio.h>
#include <SDL_ttf.h>

#include "Game.h"
#include "Players.h"
#include "Menu.h"
#include "settings.h"


void gra() {
	Game* game = new Game();
	const int FPS = 30;
	const int frame_delay = 1000 / FPS;
	Uint32 frame_start = 0;
	int frame_time;

/******************************************************************************/
/*			   					  WildPoke								      */
/******************************************************************************/
	WildPokemon poke1(1, 3, 1, 1), poke2(1, 3, 1, 1), poke3(1, 3, 1, 1), poke4(3, 1, 1, 1);
	WildPokemon poke7(17, 7, 3, 1), poke6(16, 7, 3, 1), poke5(15, 7, 3, 1);
	poke1.setCharmander();
	poke2.setPidgey();
	poke3.setBulbazaur();
	poke4.setBulbazaur();
	poke5.setBulbazaur();
	poke6.setSquirtle();
	poke7.setCharmander();
	std::vector<WildPokemon> vec{ poke1, poke2, poke3, poke4, poke5, poke6, poke7 };

	
/******************************************************************************/
/*			   					  MinePoke								      */
/******************************************************************************/
	Pokemon p1, p2, p3, p4, p5;
	p1.setCharmander();
	p2.setCharmander();
	p3.setPikachu();
	p4.setPidgey();
	p5.setSquirtle();
	std::vector<Pokemon> vecP{ p1, p2, p3, p4, p5};

	game->init_window(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size_window_x, size_window_y, false);
	game->init_game(vec);
	game->give_player_poke(vecP);

	while (game->running()) {
		frame_start = SDL_GetTicks();

		game->game_handle();

		if (game->met_poke()) {
			std::cout << "walska\n";
			game->walka();
		}

		if (game->met_NPC()) {
			game->talk_to();
		}

		game->change_map();

		game->update();
		game->render();

		game->game_end();

		frame_time = SDL_GetTicks() - frame_start;
		if (frame_delay > frame_time) SDL_Delay(frame_delay - frame_time);
	}

	if (game->finished_game() && !game->has_quited() && !game->has_pressed_esc()) {
		game->display_end_scene();
	}

	game->neg_esc();

	game->clear();
}


class Si {
public:
	int x, y;
	Si() { x = y = 0; }
	Si(int xx) { x = y = xx; }
};

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
	bool flag = true, end_of_ends = false;
	Game* game = new Game();
	const int FPS = 60;
	const int frame_delay = 1000 / FPS;
	Uint32 frame_start = 0;
	int frame_time;
	
	
	/******************************************************************************/
	/*			   					  WildPoke								      */
	/******************************************************************************/
	WildPokemon poke1(1, 3, 1, 1), poke2(1, 3, 1, 1), poke3(1, 3, 1, 1), poke4(3, 1, 1, 1), poke5(15, 7, 3, 1), poke6(16, 7, 3, 1), poke7(17, 7, 3, 1);
	WildPokemon poke8(1, 1, 2, 1), poke9(18, 1, 2, 1), poke10(18, 10, 2, 1), poke11(10, 18, 2, 1), poke12(18, 18, 2, 1), poke13(1, 1, 1, 1), poke14(1, 1, 1, 1), 
		poke15(1, 1, 1, 1), poke16(1, 1, 1, 1), poke17(1, 1, 1, 1), poke18(1, 1, 1, 1), poke19(1, 1, 1, 1), poke20(1, 1, 1, 1), poke21(1, 1, 1, 1), poke22(1, 1, 1, 1), 
		poke23(1, 1, 1, 1), poke24(1, 1, 1, 1), poke25(1, 1, 1, 1), poke26(1, 1, 1, 1), poke27(1, 1, 1, 1), poke28(1, 1, 1, 1), poke29(1, 1, 1, 1), poke30(1, 1, 1, 1);
	
	poke1.setCharmander();
	poke2.setPidgey();
	poke3.setBulbazaur();
	poke4.setBulbazaur();
	poke5.setBulbazaur();
	poke6.setSquirtle();
	poke7.setCharmander();
	poke8.setCharmander();
	poke9.setCharmander();
	poke10.setCharmander();
	poke11.setCharmander();
	poke12.setMew();
	std::vector<WildPokemon> vec{ poke1, poke2, poke3, poke4, poke5, poke6, poke7, poke8, poke9, poke10, poke11, poke12 };

	/******************************************************************************/
	/*			   					  MinePoke								      */
	/******************************************************************************/
	Pokemon p1, p2, p3, p4, p5;
	p2.setSquirtle();
	p1.setCharmander();
	p3.setPikachu();
	p4.setPidgey();
	p5.setSquirtle();
	std::vector<Pokemon> vecP{ p1, p2, p3, p4, p5 };

	game->init_window(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size_window_x, size_window_y, false);

	while (flag) {
		switch (game->menu()) {
		case 0:
			if (game->finished_game()) {
				game->init_game(vec);
				game->give_player_poke(vecP);
			}
			
			while (game->running()) {
				frame_start = SDL_GetTicks();

				game->game_handle();

				if (game->met_poke())
					game->walka();

				if (game->met_NPC())
					game->talk_to();	

				game->change_map();

				game->update();

				game->render();

				game->game_end();

				frame_time = SDL_GetTicks() - frame_start;
				if (frame_delay > frame_time) SDL_Delay(frame_delay - frame_time);
			}

			if (game->finished_game() && !game->has_quited() && !game->has_pressed_esc()) {
				game->display_end_scene();	
			}

			if (game->has_pressed_esc())
				game->neg_esc();

			if (game->has_quited()) {
				flag = false;
				break;
			}

			game->rerun();
			break;
		case 1:
			game->render_manual();
			break;
		case 2:
			game->render_creadits();
			break;
		case 3:
			game->init_game(vec);
			game->give_player_poke(vecP);
			break;
		case 4:
			flag = false;
			break;
		}
	}

	game->clear();
}


int main(int argc, char * argv[]){
	gra();
	system("pause");
	return 0;
}
