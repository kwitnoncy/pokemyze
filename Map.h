#pragma once
#include <SDL.h>
#include <fstream>
#include <string>
#include <Windows.h>

#include "Game.h"
#include "Game_object.h"

class Game;

class Map{
	friend Game;

	SDL_Rect src, dest;

	SDL_Texture* dirt;
	SDL_Texture* grass;
	SDL_Texture* bush;
	SDL_Texture* water;
	SDL_Texture* nothing;
	SDL_Texture* house;
	SDL_Texture* brick;
	SDL_Texture* path;
	
	int temp_map[20][20];
	int map[20][20];
	int xpos;
	int ypos;

	bool map_vision[20][20];

public:
	Map();
	~Map();
	void Load_map();
	void Draw_map();
	void read_map(int x, int y);
	void read_map_to_change();
	void change_map();
};