#include "Texture_manager.h"


SDL_Texture* Tex_manager::load_texture(const char* file_name) {
	SDL_Surface* tmp_surf = IMG_Load(file_name);
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::renderer, tmp_surf);
	SDL_FreeSurface(tmp_surf);
	return tex;
}

void Tex_manager::Draw(SDL_Texture * tex, SDL_Rect src, SDL_Rect dest){
//	dest.h *= size_multiply;
//	dest.w *= size_multiply;
	SDL_RenderCopy(Game::renderer, tex, &src, &dest);
}
