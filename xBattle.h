#pragma once

#include <array>

#include "Pokemon.h"
#include "Players.h"
#include "Texture_manager.h"

class xBattle {
	bool run = false;
	bool no_poke = false;
	std::array<std::array<float, 15>, 15> type_relations{
		{ { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 0, 1 },
	{ 1, 2, 0.5, 1, 0.5, 1, 1, 1, 2, 1, 1, 1, 2, 1, 0.5 },
	{ 1, 1, 2, 0.5, 0.5, 1, 1, 1, 0, 2, 1, 1, 1, 1, 0.5 },
	{ 1, 0.5, 2, 1, 0.5, 1, 1, 0.5, 2, 0.5, 1, 0.5, 2, 1, 0.5 },
	{ 1, 1, 0.5, 1, 2, 0.5, 1, 1, 2, 2, 1, 1, 1, 1, 2 },
	{ 2, 1, 1, 1, 1, 2, 1, 0.5, 1, 0.5, 0.5, 0.5, 2, 0, 1 },
	{ 1, 1, 1, 1, 2, 1, 1, 0.5, 0.5, 1, 1, 2, 0.5, 0.5, 1 },
	{ 1, 2, 1, 2, 0.5, 1, 1, 2, 1, 0, 1, 0.5, 2, 1, 1 },
	{ 1, 1, 1, 0.5, 2, 1, 2, 1, 1, 1, 1, 2, 0.5, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 0.5, 1, 1, 1, 1 },
	{ 1, 0.5, 1, 1, 2, 1, 0.5, 2, 1, 0.5, 2, 1, 1, 1, 1 },
	{ 1, 2, 1, 1, 1, 2, 0.5, 1, 0.5, 2, 1, 2, 1, 1, 1 },
	{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 2, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 }, }};
	
	
	SDL_Texture* runn;
	SDL_Texture* picked_runn;

public:
	xBattle() = default;

	void pick_skill_player(Pokemon& p1);
	void pick_skill_enemy(Pokemon& p2, const Pokemon& p1);
	void give_damage(Pokemon& a, Pokemon& d);

	float att_def (const Pokemon& a, const Pokemon& d);
	float get_eff(const Pokemon& a, const Pokemon& d);
	float get_STAB(const Pokemon& a);
	float dmg_possible_val(const Pokemon& a, const Pokemon& d);

	bool fight(Pokemon& p1, Pokemon& p2);
	bool fight(Player* pla, const Pokemon& p22);
	bool get_run();
	bool get_no_poke();

	void print_fight(const Pokemon& p1, const Pokemon& p2);
	void print_fight_back();
	void print_fight_pokes(const Pokemon& p1, const Pokemon& p2);
	void print_fight_hud(const Pokemon& p1, const Pokemon& p2);
	void print_fight_skills(const Pokemon& p1, int move);
};