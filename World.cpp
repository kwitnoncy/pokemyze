//
// Created by kwiat on 04.03.2018.
//

#include "World.h"

World::World() {
	for (unsigned int i = 0; i < 20; i++) {
		std::vector<char> pomC;
		std::vector<bool> pomB;
		for (unsigned int j = 0; j < 20; j++) {
			pomC.push_back('X');
			pomB.push_back(false);
		}
		map.push_back(pomC);
		can_go.push_back(pomB);
	}
	X_cord = 0;
	Y_cord = 0;
}

World::World(const int &new_X_cord, const int &new_Y_cord) {
	X_cord = new_X_cord;
	Y_cord = new_Y_cord;
}

int World::set_map() {
	std::ifstream input;
	std::string filename, buff, xx, yy;

	switch (X_cord) {
	case 0:
		xx = "1";
		break;
	case 1:
		xx = "2";
		break;
	case 2:
		xx = "3";
		break;
	default:
		return MAP_ERROR;
	}
	switch (Y_cord) {
	case 0:
		yy = "1";
		break;
	case 1:
		yy = "2";
		break;
	case 2:
		yy = "3";
		break;
	default:
		return MAP_ERROR;
	}

	filename = xx + yy + "n.txt";

	input.open(filename);
	//input.open("00.txt");               //      otwiera pusta mape do testow

	if (input.is_open()) {
		for (unsigned int i = 0; i < 20; i++) {
			std::vector<char> pom_map;
			std::getline(input, buff);

			for (unsigned int j = 0; j < 20; j++) {
				map[i][j] = buff[j];

				if (map[i][j] != '#')
					can_go[i][j] = true;
				else
					can_go[i][j] = false;
			}
		}
	}

	input.close();

	return 0;
}

void World::displayMap() {
	for (unsigned int i = 0; i < 20; i++) {
		for (unsigned int j = 0; j < 20; j++) {
			std::cout << map[i][j];
		}
		std::cout << "\n";
	}
}

void World::set_player_on_map(const Player& p1) {
	for (unsigned int i = 0; i < map.size(); i++) {
		for (unsigned int j = 0; j < map[i].size(); j++) {
			if (map[i][j] == 'O' && i != 0 && i != 19 && j != 0 && j != 19) {
				map[i][j] = '.';
				break;
			}
			else if ((map[i][j] == 'O') && (i == 0 || i == 19 || j == 0 || j == 19)) {
				map[i][j] = '_';
			}
		}
	}

//	map[p1.pos_Y][p1.pos_X] = 'O';
}

void World::change_map(Player &p1) {
/*	if (p1.pos_X == 0) {
		X_cord--;
		p1.pos_X = 18;
		set_map();
	}
	else if (p1.pos_X == 19) {
		X_cord++;
		p1.pos_X = 1;
		set_map();
	}
	else if (p1.pos_Y == 0) {
		Y_cord--;
		p1.pos_Y = 18;
		set_map();
	}
	else if (p1.pos_Y == 19) {
		Y_cord++;
		p1.pos_Y = 1;
		set_map();
	}
	*/
}

bool World::end_game(const Player &p1) {
/*	if (map[p1.pos_Y][p1.pos_X] == '+')
		return true;
	*/
	return false;
}

char World::pole(Player &p1) {
//	return map[p1.pos_Y][p1.pos_X];
	return '#';
}