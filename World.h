#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <Windows.h>

#include "Players.h"


#define MAP_ERROR (-999)

class Player;
class Game;
class Battle;

class World {
	friend Player;
	friend Battle;
	friend Game;

	std::vector<std::vector<char>> map;
	std::vector<std::vector<bool>> can_go;
	int X_cord;
	int Y_cord;

public:
	World();
	World(const int& new_X_cord, const int& new_Y_cord);
	int set_map();
	void displayMap();
	void set_player_on_map(const Player& p1);
	void change_map(Player& p1);
	char pole(Player& p1);
	bool end_game(const Player& p1);
	void print() {
		std::cout << "X " << X_cord << " Y " << Y_cord << "\n";
	}
};