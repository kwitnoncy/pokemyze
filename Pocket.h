#pragma once

#include <vector>

#include "Item.h"

//	TODO
class Pocket {
	std::vector<Item*> vec;

public:
	Pocket() = default;
	~Pocket() = default;
	
	void add_item(Item* ptr);
	void show_items();

};