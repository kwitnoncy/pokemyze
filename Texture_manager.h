#pragma once

#include <Windows.h>

#include "Game.h"
#include "settings.h"

class Tex_manager {
public:
	static SDL_Texture* load_texture(const char* file_name);
	static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);

};