#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <conio.h>
#include <synchapi.h>
#include <Windows.h>

#include "Pokemon.h"
#include "World.h"

class World;
class Battle;
class xBattle;
class Game;

class Player{
	friend World;
	friend Battle;
	friend Game;
	friend xBattle;

protected:
	int lvl;
	int pos_X;
	int pos_Y;
	std::string name;
	std::vector<Pokemon> pokemons;
	int potions = 5;

public:
	Player();
	Player(const std::string& new_name, const int& new_lvl, const std::vector<Pokemon>& vecP);
	bool move(World& temp_world);

	void print_poke();
	void heal();
	void print() {
		std::cout << pos_X << " " << pos_Y << "\n";
	}
};