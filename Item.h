#pragma once

#include <string>
#include <Windows.h>


#include "Players.h"
#include "settings.h"

enum Popularity {
	Common,
	Rare,
	Legendary,
};

enum Type {
	Error_type,
	Potion_type,
	Pokeball_type,
	Food_type,
	Perk_type,
};

class Item {
	friend Game;

protected:
	std::string name;
	int xpos;
	int ypos;
	int map_xpos;
	int map_ypos;
	int value;
	bool picked_up;
	bool ignored;
	Popularity popularity;
	Type type;
	SDL_Texture* tex;

public:
	~Item() = default;
	Item(std::string xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, int xvalue, Popularity xpop);
	virtual std::string get_name();
};

class Potion : public Item {
	friend Game;

public:
	Potion() = default;
	Potion(std::string xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, int xvalue, Popularity xpop);
};