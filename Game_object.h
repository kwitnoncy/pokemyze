#pragma once

#include <Windows.h>

#include "Game.h"
#include "Players.h"
#include "settings.h"

class Game;

struct Side{
	bool upper;
	bool down;
	bool left;
	bool right;

	Side() {
		down = true;
		upper = false;
		left = false;
		right = false;
	}
	void operator= (bool flag) {
		down = flag;
		upper = flag;
		left = flag;
		right = flag;
	}
};

class Game_object : Player{
	friend Game;

	int xpos;
	int ypos;
	SDL_Texture* obj_tex;
	SDL_Texture* obj_tex_upper;
	SDL_Texture* obj_tex_down;
	SDL_Texture* obj_tex_left;
	SDL_Texture* obj_tex_right;
	SDL_Rect src_rect;
	SDL_Rect dest_rect;

public:
	Side side;
	Game_object() = default;
	~Game_object() = default;
	Game_object(const char* texture_sheet, int x, int y);
	Game_object(const char* texture_sheet, int x, int y, std::string xname);

	void update();
	void render();
};

class NPC {
	friend Game;

	std::string name;
	std::string line;
	std::vector<std::string> senteces;
	int xpos;
	int ypos;
	int map_xpos;
	int map_ypos;
	bool action;
	bool visited;

	SDL_Texture* tex;
	SDL_Texture* tex_up;
	SDL_Texture* tex_down;
	SDL_Texture* tex_left;
	SDL_Texture* tex_right;
	SDL_Rect src;
	SDL_Rect dest;

public:
	NPC();
	NPC(const std::string& xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, bool xaction, const char* path);
	NPC(const std::string& xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, bool xaction, std::string linex, const char* path);
	
	void render();

	bool is_action();
	void add_phrase(std::string phrase);
	
	Side side;
};