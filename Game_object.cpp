#include<iostream>
#include<SDL.h>

#include "Game_object.h"
#include "Texture_manager.h"

/****************************************************************************************************************************/
/*																Game Object													*/	
/****************************************************************************************************************************/
Game_object::Game_object(const char* texture_sheet, int x, int y) {
	obj_tex = Tex_manager::load_texture(texture_sheet);

	obj_tex_down = Tex_manager::load_texture("assets/MyPlayerF.png");
	obj_tex_upper = Tex_manager::load_texture("assets/MyPlayerB.png");
	obj_tex_left = Tex_manager::load_texture("assets/MyPlayerL.png");
	obj_tex_right = Tex_manager::load_texture("assets/MyPlayerR.png");

	xpos = y;
	ypos = x;
}

Game_object::Game_object(const char* texture_sheet, int x, int y, std::string xname) {
	obj_tex = Tex_manager::load_texture(texture_sheet);

	obj_tex_down = Tex_manager::load_texture("assets/MyPlayerF.png");
	obj_tex_upper = Tex_manager::load_texture("assets/MyPlayerB.png");
	obj_tex_left = Tex_manager::load_texture("assets/MyPlayerL.png");
	obj_tex_right = Tex_manager::load_texture("assets/MyPlayerR.png");

	xpos = y;
	ypos = x;

	name = xname;
}

void Game_object::update() {
	src_rect.h = 32;
	src_rect.w = 32;
	src_rect.x = 0;
	src_rect.y = 0;

	dest_rect.x = xpos;
	dest_rect.y = ypos;
	dest_rect.w = src_rect.w * size_multiply;
	dest_rect.h = src_rect.h * size_multiply;
}

void Game_object::render() {
	if (side.down)
		obj_tex = obj_tex_down;
	else if (side.upper)
		obj_tex = obj_tex_upper;
	else if (side.left)
		obj_tex = obj_tex_left;
	else if (side.right)
		obj_tex = obj_tex_right;

	SDL_RenderCopy(Game::renderer, obj_tex, &src_rect, &dest_rect);
}


/****************************************************************************************************************************/
/*																	NPC														*/
/****************************************************************************************************************************/
NPC::NPC() {
	name = "";
	xpos = 0;
	ypos = 0;
	map_xpos = 0;
	map_ypos = 0;
	action = 0;
	visited = false;
	side = false;

	std::cout << "Blad tworzenia NPC\n";
}

NPC::NPC(const std::string& xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, bool xaction, const char* path) {
	name = xname;
	xpos = xxpos;
	ypos = xypos;
	map_xpos = xmap_xpos;
	map_ypos = xmap_ypos;
	action = xaction;
	visited = false;
	side = false;

	tex = Tex_manager::load_texture(path);
	src.x = src.y = 0;
	src.w = src.h = 32 * size_multiply;
	dest.x = xxpos * 32 * size_multiply;
	dest.y = xypos * 32 * size_multiply;
	dest.h = dest.w = 32 * size_multiply;
}

NPC::NPC(const std::string& xname, int xxpos, int xypos, int xmap_xpos, int xmap_ypos, bool xaction, std::string linex, const char* path) {
	name = xname;
	xpos = xxpos;
	ypos = xypos;
	map_xpos = xmap_xpos;
	map_ypos = xmap_ypos;
	action = xaction;
	visited = false;
	side = false;
	line = linex;

	tex = Tex_manager::load_texture(path);
	src.x = src.y = 0;
	src.w = src.h = 32 * size_multiply;
	dest.x = xxpos * 32 * size_multiply;
	dest.y = xypos * 32 * size_multiply;
	dest.h = dest.w = 32 * size_multiply;
}

bool NPC::is_action() {
	return action;
}

void NPC::render() {
	if (side.down)
		tex = tex_down;
	else if (side.upper)
		tex = tex_up;
	else if (side.left)
		tex = tex_left;
	else if (side.right)
		tex = tex_right;
	

	SDL_RenderCopy(Game::renderer, tex, &src, &dest);
}

void NPC::add_phrase(std::string phrase) {
	senteces.push_back(phrase);
}