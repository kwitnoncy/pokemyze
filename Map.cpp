#include "Map.h"
#include "Texture_manager.h"

Map::Map() {
	dirt = Tex_manager::load_texture("assets/dirt.png");
	grass = Tex_manager::load_texture("assets/grass.png");
	bush = Tex_manager::load_texture("assets/bush.png");
	water = Tex_manager::load_texture("assets/water.png");
	nothing = Tex_manager::load_texture("assets/nic.png");
	//house = Tex_manager::load_texture("assets/domek_do_Julki.png");
	house = Tex_manager::load_texture("assets/dom2.png");
	path = Tex_manager::load_texture("assets/pavment.png");
	
	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 20; j++) {
			map_vision[i][j] = true;
		}
	}

//	xpos = 3;					//	Debugging: cordy mapy z bossem
//	ypos = 1;					//	Debugging: cordy mapy z bossem

	xpos = 999;
	ypos = 999;

	read_map(xpos, ypos);

	Load_map();

	src.x = src.y = 0;
	src.h = src.w = 32;
	dest.h = dest.w = 32;
	dest.x = dest.y = 0;
}

Map::~Map() {}

void Map::read_map(int x, int y) {
	std::ifstream input;
	std::string temp, xx, yy, name;

	switch (x) {
	case 0:
		xx = '0';
		break;
		case 1:
			xx = '1';
			break;
		case 3:
			xx = '3';
			break;
		case 2:
			xx = '2';
			break;
		case 999:
			xx = "99";
			break;
	}
	switch (y) {
		case 0:
			xx = '0';
			break;
		case 1:
			yy = '1';
			break;
		case 3:
			yy = '3';
			break;
		case 2:
			yy = '2';
			break;
		case 999:
			yy = "9";
			break;
	}
	
	name = "x" + xx + yy + ".txt";
	input.open(name);

	for (int i = 0; i < 20; i++) {
		std::getline(input, temp);
		for (int j = 0; j < 20; j++) {
			switch (temp[j]){
				case '0':
					temp_map[i][j] = 0;
					break;
				case '1':
					temp_map[i][j] = 1;
					break;
				case '2':
					temp_map[i][j] = 2;
					break;
				case '3':
					temp_map[i][j] = 3;
					break;
				case '4':
					temp_map[i][j] = 4;
					break;
				case '5':
					temp_map[i][j] = 5;
					break;
				case '6':
					temp_map[i][j] = 6;
					break;
				case '7':
					temp_map[i][j] = 7;
					break;
				case '8':
					temp_map[i][j] = 8;
					break;
				case '9':
					temp_map[i][j] = 9;
					break;
				default:
					temp_map[i][j] = 999;
			}
		}
	}

	input.close();
}

void Map::Load_map() {
	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 20; j++) {
			map[i][j] = temp_map[i][j];
		}
	}
}

void Map::Draw_map() {
	int type = 0;

	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 20; j++) {
			type = map[i][j];

			dest.x = j * 32;
			dest.y = i * 32;
			switch (type) {
			case 0:
				Tex_manager::Draw(grass, src, dest);
				break;
			case 1:
				Tex_manager::Draw(water, src, dest);
				break;
			case 2:
				Tex_manager::Draw(bush, src, dest);
				break;
			case 3:
				Tex_manager::Draw(dirt, src, dest);
				break;
			case 4:
				Tex_manager::Draw(path, src, dest);
				break;
			default:
				Tex_manager::Draw(nothing, src, dest);
				break;
			}
		}
	}

	bool kon = false;
	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 20; j++) {
			if (map[i][j] == 9) {
				SDL_Rect srch, desth;
				srch.x = 0;
				srch.y = 0;
				srch.w = 192;
				srch.h = 160;

				desth.x = 32 * j;
				desth.y = 32 * i;
				desth.w = 192;
				desth.h = 160;
				Tex_manager::Draw(house, srch, desth);
				kon = true;
			}

			if (kon) {
				break;
			}
		}
		
		if (kon) {
			break;
		}
	}
}

void Map::change_map() {
	read_map_to_change();
	Load_map();
}

void Map::read_map_to_change() {
	std::ifstream input;
	std::string temp, xx, yy, name;

	switch (xpos) {
	case 0:
		xx = '0';
		break;
	case 1:
		xx = '1';
		break;
	case 3:
		xx = '3';
		break;
	case 2:
		xx = '2';
		break;
	}
	switch (ypos) {
	case 0:
		yy = '0';
		break;
	case 1:
		yy = '1';
		break;
	case 3:
		yy = '3';
		break;
	case 2:
		yy = '2';
		break;
	}

	name = "x" + xx + yy + ".txt";

	if (xpos == 999 && ypos == 999)
		name = "x999.txt";

	input.open(name);
	
	std::cout << "file name: " << name << "\n";

	for (int i = 0; i < 20; i++) {
		std::getline(input, temp);
		for (int j = 0; j < temp.size(); j++) {
			switch (temp[j]) {
			case '0':
				temp_map[i][j] = 0;
				break;
			case '1':
				temp_map[i][j] = 1;
				break;
			case '2':
				temp_map[i][j] = 2;
				break;
			case '3':
				temp_map[i][j] = 3;
				break;
			case '4':
				temp_map[i][j] = 4;
				break;
			case '5':
				temp_map[i][j] = 5;
				break;
			case '6':
				temp_map[i][j] = 6;
				break;
			case '7':
				temp_map[i][j] = 7;
				break;
			case '8':
				temp_map[i][j] = 8;
				break;
			case '9':
				temp_map[i][j] = 9;
				break;
			default:
				temp_map[i][j] = 999;
			}
		}
	}

	input.close();
}

