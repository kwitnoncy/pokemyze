#include "Menu.h"

SDL_Renderer* Main_menu::menu_renderer = nullptr;

void Main_menu::init(const char* title, int xpos, int ypos, int width, int height, bool full_screen) {
	int flags = 0;
	bool good = true;

	if (full_screen)
		flags = SDL_WINDOW_FULLSCREEN;

	if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
		std::cout << "SDL initialised...\n";

		window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);

		if (window)
			std::cout << "Window created...\n";

		menu_renderer = SDL_CreateRenderer(window, -1, 0);

		if (menu_renderer) {
			SDL_SetRenderDrawColor(menu_renderer, 255, 255, 255, 255);
			std::cout << "Renderer created...\n";
		}
	}

	game = load_texture("assets/menu_game.png");
	picked_game = load_texture("assets/picked_menu_game.png");
	if (game == nullptr || picked_game == nullptr) {
		good = false;
		std::cout << "gra\n";
	}

	manual = load_texture("assets/menu_manual.png");
	picked_manual = load_texture("assets/picked_menu_manual.png");
	if (manual == nullptr || picked_manual == nullptr) {
		good = false;
		std::cout << "man\n";
	}

	credits = load_texture("assets/menu_credits.png");
	picked_credits = load_texture("assets/picked_menu_credits.png");
	if (credits == nullptr || picked_credits == nullptr) {
		good = false;
		std::cout << "cred\n";
	}

	quit = load_texture("assets/menu_quit.png");
	picked_quit = load_texture("assets/picked_menu_quit.png");
	if (quit == nullptr || picked_quit == nullptr) {
		good = false;
	}

	if (good)
		open = true;
}

int Main_menu::pick_option() {
	bool running = true, first = true;
	int posy = 0;
	
	option = posy;

	draw_menu();

	while (running) {
		SDL_Event* event_skill = new SDL_Event();
		SDL_PollEvent(event_skill);

		switch (event_skill->type) {
		case SDL_KEYDOWN:
			if (event_skill->key.keysym.sym == SDLK_UP) {
				posy--;
				if (posy == -1)
					posy = 0;
				break;
			}
			else if (event_skill->key.keysym.sym == SDLK_DOWN) {
				posy++;
				if (posy == 4)
					posy = 3;
				break;
			}
			else if (event_skill->key.keysym.sym == SDLK_RETURN or event_skill->key.keysym.sym == SDLK_KP_ENTER) {
				running = false;
			}
		}

		option = posy;

		draw_menu();

		delete event_skill;
	}

	return posy;
}

void Main_menu::clear() {
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(menu_renderer);
}

void Main_menu::draw_menu() {
	SDL_Rect src, dest;
	
	src.x = 0;
	src.y = 0;
	src.h = 64;
	src.w = 256;
	
	dest.h = 64;
	dest.w = 256;
	dest.x = 192;

	SDL_RenderClear(menu_renderer);

	dest.y = 320;
	if (option == 0)
		draw(picked_game, src, dest);
	else
		draw(game, src, dest);

	dest.y += 64;
	if (option == 1)
		draw(picked_manual, src, dest);
	else
		draw(manual, src, dest);

	dest.y += 64;
	if (option == 2)
		draw(picked_credits, src, dest);
	else
		draw(credits, src, dest);

	dest.y += 64;
	if (option == 3)
		draw(picked_quit, src, dest);
	else
		draw(quit, src, dest);
	
	SDL_RenderPresent(menu_renderer);
}

SDL_Texture* Main_menu::load_texture(const char* file_name){
	SDL_Surface* tmp_surf = IMG_Load(file_name);
	SDL_Texture* tex = SDL_CreateTextureFromSurface(menu_renderer, tmp_surf);
	SDL_FreeSurface(tmp_surf);

	return tex;
}

bool Main_menu::is_open() {
	return open;
}

void Main_menu::quit_menu() {
	open = false;
}

void Main_menu::draw(SDL_Texture * tex, SDL_Rect src, SDL_Rect dest) {
	SDL_RenderCopy(menu_renderer, tex, &src, &dest);
}