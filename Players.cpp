//
// Created by kwiat on 04.03.2018.
//

#include <Windows.h>
#include "Players.h"

Player::Player() {
	name = "";
	lvl = 1;
	pos_X = 1;
	pos_Y = 1;
}

Player::Player(const std::string &new_name, const int &new_lvl, const std::vector<Pokemon> &vecP) {
	name = new_name;
	lvl = new_lvl;
	pos_X = 1;
	pos_Y = 1;

	for (const auto& x : vecP) {
		pokemons.push_back(x);
	}
}

bool Player::move(World &temp_world) {
	int c = _getch();

	switch (c) {
	case 72:
		pos_Y--;
		if (pos_Y == -1)
			pos_Y = 0;

		if (!temp_world.can_go[pos_Y][pos_X])
			pos_Y++;

		return true;
	case 80:
		pos_Y++;
		if (pos_Y == 20)
			pos_Y = 19;

		if (!temp_world.can_go[pos_Y][pos_X])
			pos_Y--;

		return true;
	case 75:
		pos_X--;
		if (pos_X == -1)
			pos_X = 0;

		if (!temp_world.can_go[pos_Y][pos_X])
			pos_X++;

		return true;
	case 77:
		pos_X++;
		if (pos_X == 20)
			pos_X = 19;

		if (!temp_world.can_go[pos_Y][pos_X])
			pos_X--;

		return true;
	case 112:
		print_poke();
		_getch();
		return true;
	case 105:
		heal();
		return true;
	case 27:
		return false;
	default:
		return true;
	}
}

void Player::print_poke() {
	
}

void Player::heal() {
	if (potions > 0) {
		for (auto x : pokemons) {
			x.heal_poke(20);
		}
	}
	Sleep(2000);
	potions--;
}