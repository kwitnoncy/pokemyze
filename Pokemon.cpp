//
// Created by kwiat on 03.03.2018.
//
#include "Pokemon.h"
#include "Game.h"
#include "Texture_manager.h"

/*--------------------------------------------------------------------------------------------------------------------*/
/*                                              Pokemon functions                                                     */
/*--------------------------------------------------------------------------------------------------------------------*/
bool Pokemon::has_health_points() {
	if (health_points > 0)
		return true;
	else
		return false;
}

int Pokemon::get_health_points() {
	return health_points;
}

int Pokemon::get_max_health_points() {
	return base_health_points;
}

void Pokemon::print_HP() {
	std::cout << name << ": " << health_points << "\n";
}

void Pokemon::heal_poke(int x) {
	std::cout << "healing: " << this->name << "\n";
	if (this->health_points + x <= this->base_health_points)
		this->health_points += x;
	else
		this->health_points = this->base_health_points;
}

std::string Pokemon::get_name() {
	return name;
}

void Pokemon::load_teture(const char* filename) {

	SDL_Surface* tmp_surf = IMG_Load(filename);
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::renderer, tmp_surf);
	SDL_FreeSurface(tmp_surf);

	texture = tex;
}

void Pokemon::take_dmg(const int& x) {
	health_points -= x;
}

bool Pokemon::is_texture() {
	if (texture == nullptr)
		return false;
	else
		return true;
}

/*--------------------------------------------------------------------------------------------------------------------*/
/*                                              Deleraction of Pokemons                                               */
/*--------------------------------------------------------------------------------------------------------------------*/
Pokemon::Pokemon() {
	name = "";
	type = 0;
	health_points = 0;
	base_health_points = 0;
	experience_points = 0;
	base_attack = 0;
	base_defence = 0;
	special_attack = 0;
	special_defence = 0;
	level = 0;
	evolution = 0;
	move_pick = 0;
}

void Pokemon::setCharmander() {
	name = "Charmander";
	type = 2;
	health_points = 39;
	base_health_points = 39;
	base_attack = 52;
	base_defence = 43;
	special_attack = 60;
	special_defence = 50;
	level = 3;
	id = 1;

	move[0].name = "Ember";
	move[0].type = 2;
	move[0].damage = 40.0;
	move[0].target = true;
	move[0].SPECIAL = true;

	move[1].name = "Scratch";
	move[1].type = 1;
	move[1].damage = 40.0;
	move[1].target = true;
	move[1].SPECIAL = false;

	move[2].name = "Tackle";
	move[2].type = 1;
	move[2].damage = 50.0;
	move[2].target = true;
	move[2].SPECIAL = false;

	move[3].name = "Quick Attack";
	move[3].type = 1;
	move[3].damage = 40.0;
	move[3].target = true;
	move[3].SPECIAL = false;
}

void Pokemon::setBulbazaur() {
	name = "Bulbasaur";
	type = 5;
	health_points = 20;			//	TESTY powinno byc 45
	base_health_points = 20;	//	TESTY powinno byc 45
	base_attack = 49;
	base_defence = 49;
	special_attack = 65;
	special_defence = 65;
	level = 3;
	id = 2;

	move[0].name = "Vine Whip";
	move[0].type = 5;
	move[0].damage = 35.0;
	move[0].target = true;
	move[0].SPECIAL = false;

	move[1].name = "Scratch";
	move[1].type = 1;
	move[1].damage = 40.0;
	move[1].target = true;
	move[1].SPECIAL = false;
	
	move[2].name = "Tackle";
	move[2].type = 1;
	move[2].damage = 50.0;
	move[2].target = true;
	move[2].SPECIAL = false;
	
	move[3].name = "Quick Attack";
	move[3].type = 1;
	move[3].damage = 40.0;
	move[3].target = true;
	move[3].SPECIAL = false;
}

void Pokemon::setSquirtle() {
	name = "Squirtle";
	type = 3;
	health_points = 45;
	base_health_points = 45;
	base_attack = 48;
	base_defence = 65;
	special_attack = 65;
	special_defence = 65;
	level = 3;
	id = 3;

	move[0].name = "Tackle";
	move[0].type = 1;
	move[0].damage = 35;
	move[0].target = true;
	move[0].SPECIAL = false;

	move[1].name = "Bubble";
	move[1].type = 3;
	move[1].damage = 20;
	move[1].target = true;
	move[1].SPECIAL = true;

	move[2].name = "Water Gun";
	move[2].type = 3;
	move[2].damage = 40;
	move[2].target = true;
	move[2].SPECIAL = true;

	move[3].name = "Bite";
	move[3].type = 1;
	move[3].damage = 60;
	move[3].target = true;
	move[3].SPECIAL = false;
}

void Pokemon::setPidgey(){
	name = "Pidgey";
	type = 1;
	health_points = 40;
	base_health_points = 40;
	base_attack = 45;
	base_defence = 40;
	special_attack = 65;
	special_defence = 65;
	level = 3;
	id = 4;

	move[0].name = "Gust";
	move[0].type = 1;
	move[0].damage = 40;
	move[0].target = true;
	move[0].SPECIAL = true;

	move[1].name = "Quick Attack";
	move[1].type = 1;
	move[1].damage = 40;
	move[1].target = true;
	move[1].SPECIAL = true;

	move[2].name = "Wind Attack";
	move[2].type = 1;
	move[2].damage = 35;
	move[2].target = true;
	move[2].SPECIAL = true;

	move[3].name = "Razor Wind";
	move[3].type = 1;
	move[3].damage = 40;
	move[3].target = true;
	move[3].SPECIAL = false;
}

void Pokemon::setPikachu(){
	name = "Pikachu";
	type = 4;
	health_points = 35;
	base_health_points = 35;
	base_attack = 55;
	base_defence = 30;
	special_attack = 50;
	special_defence = 50;
	level = 3;

	move[0].name = "Thunder Shock";
	move[0].type = 8;
	move[0].damage = 40;
	move[0].target = true;
	move[0].SPECIAL = true;

	move[1].name = "Quick Attack";
	move[1].type = 1;
	move[1].damage = 40;
	move[1].target = true;
	move[1].SPECIAL = false;

	move[2].name = "Thunder Bolt";
	move[2].type = 8;
	move[2].damage = 95;
	move[2].target = true;
	move[2].SPECIAL = true;

	move[3].name = "Thunder";
	move[3].type = 8;
	move[3].damage = 120;
	move[3].target = true;
	move[3].SPECIAL = true;
}

void Pokemon::setMew(){
	name = "MEW";
	type = 4;
	health_points = 100;
	base_health_points = 100;
	base_attack = 100;
	base_defence = 100;
	special_attack = 100;
	special_defence = 100;
	level = 7;

	move[0].name = "Psychic";
	move[0].type = 6;
	move[0].damage = 90;
	move[0].target = true;
	move[0].SPECIAL = true;

	move[1].name = "Shock";
	move[1].type = 6;
	move[1].damage = 90;
	move[1].target = true;
	move[1].SPECIAL = true;

	move[2].name = "Punch";
	move[2].type = 6;
	move[2].damage = 90;
	move[2].target = true;
	move[2].SPECIAL = true;

	move[3].name = "Quick Attack";
	move[3].type = 6;
	move[3].damage = 90;
	move[3].target = true;
	move[3].SPECIAL = true;
}

/*--------------------------------------------------------------------------------------------------------------------*/
/*                                          Deleraction of WildPokemons                                               */
/*--------------------------------------------------------------------------------------------------------------------*/
WildPokemon::WildPokemon() {
	pos_X = 0;
	pos_Y = 0;
	map_pos_X = 0;
	map_pos_Y = 0;
	wild = true;
}

WildPokemon::WildPokemon(int new_pos_X, int new_pos_Y, int new_map_pos_X, int new_map_pos_Y) {
	pos_X = new_pos_X;
	pos_Y = new_pos_Y;
	map_pos_X = new_map_pos_X;
	map_pos_Y = new_map_pos_Y;
	wild = true;
}

void WildPokemon::setCharmander() {
	Pokemon::setCharmander();
}

void WildPokemon::setBulbazaur() {
	Pokemon::setBulbazaur();
}

void WildPokemon::setSquirtle() {
	Pokemon::setSquirtle();
}

void WildPokemon::setPidgey() {
	Pokemon::setPidgey();
}

void WildPokemon::setPikachu() {
	Pokemon::setPikachu();
}

void WildPokemon::setMew() {
	Pokemon::setMew();
}

void WildPokemon::print() {
	std::cout << "X " << pos_X << " Y " << pos_Y << " mX " << map_pos_X << " mY " << map_pos_Y << "\n";
}