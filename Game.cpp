#include <iostream>
#include <algorithm>
#include <string>
#include <SDL_ttf.h>

#include "Texture_manager.h"
#include "Game.h"
#include "settings.h"


SDL_Renderer* Game::renderer = nullptr;

void Game::init_window(int xpos, int ypos, int width, int height, bool full_screen) {
	int flags = 0;

	if (full_screen)
		flags = SDL_WINDOW_FULLSCREEN;


	if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
		std::cout << "SDL initialised...\n";

		window = SDL_CreateWindow("PokeMyze", xpos, ypos, width, height, flags);

		if (window)
			std::cout << "Window created...\n";

		renderer = SDL_CreateRenderer(window, -1, 0);

		if (renderer) {
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
			std::cout << "Renderer created...\n";
		}

		is_running = true;
		quited = false;
	}
	else {
		is_running = false;
	}

	TTF_Init();

	nic = Tex_manager::load_texture("assets/nic.png");

	ended = true;
	escape_pressed = false;

	game_tex = Tex_manager::load_texture("assets/menu_game.png");
	picked_game = Tex_manager::load_texture("assets/picked_menu_game.png");

	manual = Tex_manager::load_texture("assets/menu_manual.png");
	picked_manual = Tex_manager::load_texture("assets/picked_menu_manual.png");

	credits = Tex_manager::load_texture("assets/menu_credits.png");
	picked_credits = Tex_manager::load_texture("assets/picked_menu_credits.png");

	quit = Tex_manager::load_texture("assets/menu_quit.png");
	picked_quit = Tex_manager::load_texture("assets/picked_menu_quit.png");

	reset = Tex_manager::load_texture("assets/menu_reset.png");
	picked_reset = Tex_manager::load_texture("assets/picked_menu_reset.png");
}

void Game::init_game(const std::vector<WildPokemon> temp_p) {
	fight = false;

	player = new Game_object("assets/MyPlayerF.png", 18 * player_pos_multi, 10 * player_pos_multi, "Pjoter");			//	Poczatek gry
//	player = new Game_object("assets/MyPlayerF.png", 7 * player_pos_multi, 17 * player_pos_multi, "Pjoter");			//	Przed bossem
	map = new Map();
	ran_turns = 10;

	vec_poke.clear();

	for (const auto& poke : temp_p)
		vec_poke.push_back(poke);

	NPC npc_temp1("Queen", 10, 17, 0, 1, true, "assets/MyQueenF.png");
	NPC npc_temp3("Prof", 11, 10, 0, 1, true, "assets/MyProfF.png");
	NPC npc_temp4("Slave", 2, 7, 3, 1, true, "assets/slave.png");
	NPC npc_temp5("Boss", 18, 7, 3, 1, true, "assets/Boss.png");
	NPC npc_temp6("Princess", 18, 9, 3, 1, true, "assets/MyQueenF.png");

	npc_temp1.add_phrase("Hej " + player->name + " slyszales o");
	npc_temp1.add_phrase("misji polegajacej na         ");
	npc_temp1.add_phrase("uratowaniu ksiezniczki.      ");
	npc_temp1.add_phrase("Jesli nie, to odwiedz        ");
	npc_temp1.add_phrase("profesora. Zapewne znajduje  ");
	npc_temp1.add_phrase("sie gdzies na polnoc.        ");
	npc_temp1.add_phrase("Powodzenia                   ");
	
	npc_temp3.add_phrase("Ooo " + player->name + " dobrze ze ");
	npc_temp3.add_phrase("Cie widze. Sluchaj. Potrzeba  ");
	npc_temp3.add_phrase("bohatera, ktory uratuje       ");
	npc_temp3.add_phrase("ksiezniczke. Zostala ona      ");
	npc_temp3.add_phrase("uprowadzona przez podlego     ");
	npc_temp3.add_phrase("trenera pokemonow. Pomoz.     ");
	npc_temp3.add_phrase("                              ");
	npc_temp3.add_phrase("Czekaj! Po dordze beda na ciebie");
	npc_temp3.add_phrase("czekaly dzikie pokemony. Badz ");
	npc_temp3.add_phrase("ostrozny.                     ");
	npc_temp3.add_phrase("Co do lokalizacji ksiezniczki.");
	npc_temp3.add_phrase("Podobno znajduje sie na wschod");
	npc_temp3.add_phrase("stad. POWODZENIA!!!           ");

	npc_temp4.add_phrase("Ha Ha Ha Ty tez przybyles     ");
	npc_temp4.add_phrase("zeby uratowac ksiezniczke?    ");
	npc_temp4.add_phrase("Ha Ha Ha zbierzesz takie same ");
	npc_temp4.add_phrase("becki jak pozostali. Nie dasz ");
	npc_temp4.add_phrase("rady pokonac trawiastego,     ");
	npc_temp4.add_phrase("wodnego i ognistego pokemonow ");
	npc_temp4.add_phrase("UPS. chyba powiedzialem za    ");
	npc_temp4.add_phrase("duzo... Znikam                ");

	npc_temp5.add_phrase("O nie... Pokonales mnie i moje");
	npc_temp5.add_phrase("pokemony. Jak mogles... Trudno");
	npc_temp5.add_phrase("Bierz ksiezniczke i uciekaj   ");

	npc_temp6.add_phrase("YOO m8");
	npc_temp6.add_phrase("");

	vec_npc.push_back(npc_temp1);
	vec_npc.push_back(npc_temp3);
	vec_npc.push_back(npc_temp4);
	vec_npc.push_back(npc_temp5);
	vec_npc.push_back(npc_temp6);

	plot.visited_prof = false;
	plot.visited_queen = false;
	plot.fought_pokemon = false;
	plot.fought_boss = false;
	plot.left_house = false;

	ended = false;
}

void Game::game_handle() {
	SDL_Event event;
	SDL_PollEvent(&event);

	int dzielnik = 32 * size_multiply;

	if (ran) {
		std::cout << "ucieczka: " << ran_turns << "\n";
	}

	switch (event.type) {
	case SDL_QUIT:
		is_running = false;
		quited = true;
		break;
	case SDL_KEYDOWN:
		if (event.key.keysym.sym == SDLK_LEFT) {
			player->xpos -= 4;
			player->side = false;
			player->side.left = true;
			if (player->xpos < 0 || (map->map[player->ypos / dzielnik][player->xpos / dzielnik] != 0 && map->map[player->ypos / dzielnik][player->xpos / dzielnik] != 4) 
				|| (map->map[(player->ypos + 28) / dzielnik][player->xpos / dzielnik] != 0 && map->map[(player->ypos + 28) / dzielnik][player->xpos / dzielnik] != 4)) {
				player->xpos += 4;
				ran_turns++;
			}
			ran_turns--;
		}
		else if (event.key.keysym.sym == SDLK_RIGHT) {
			player->xpos += 4;
			player->side = false;
			player->side.right = true;
			if (player->xpos + 28 > (640 * size_multiply) || (map->map[player->ypos / dzielnik][(player->xpos + 28) / dzielnik] != 0 && map->map[player->ypos / dzielnik][(player->xpos + 28) / dzielnik] != 4) 
				|| (map->map[(player->ypos + 28) / dzielnik][(player->xpos + 28) / dzielnik] != 0 && map->map[(player->ypos + 28) / dzielnik][(player->xpos + 28) / dzielnik] != 4)){
				player->xpos -= 4;
				ran_turns++;
			}
			ran_turns--;
		}
		else if (event.key.keysym.sym == SDLK_DOWN) {
			player->ypos += 4;
			player->side = false;
			player->side.down = true;
			if (player->ypos + 28 >= (640  * size_multiply) || (map->map[(player->ypos + 28) / dzielnik][player->xpos / dzielnik] != 0 && map->map[(player->ypos + 28) / dzielnik][player->xpos / dzielnik] != 4) 
				|| (map->map[(player->ypos + 28) / dzielnik][(player->xpos + 31) / dzielnik] != 0 && map->map[(player->ypos + 28) / dzielnik][(player->xpos + 31) / dzielnik] != 4)){
				player->ypos -= 4;	
				ran_turns++;
			}
			ran_turns--;
		}
		else if (event.key.keysym.sym == SDLK_UP) {
			player->ypos -= 4;
			player->side = false;
			player->side.upper = true;
			if (player->ypos < 0 || (map->map[player->ypos / dzielnik][player->xpos / dzielnik] != 0 && map->map[player->ypos / dzielnik][player->xpos / dzielnik] != 4) 
				|| (map->map[player->ypos / dzielnik][(player->xpos + 28) / dzielnik] != 0 && map->map[player->ypos / dzielnik][(player->xpos + 28) / dzielnik] != 4)){
				player->ypos += 4;
				ran_turns++;
			}
			ran_turns--;
		}
		else if (event.key.keysym.sym == SDLK_p) {
			change_poke_order();
		}
		else if (event.key.keysym.sym == SDLK_ESCAPE) {
			is_running = false;
			escape_pressed = true;
			ended = false;
			break;
		}
	default:
		break;
	}
}

void Game::update() {
	player->update();
}

void Game::render() {
	SDL_RenderClear(renderer);

	map->Draw_map();
	for (NPC& x : vec_npc) {
		if (x.map_xpos == map->xpos && x.map_ypos == map->ypos)
			x.render();
	}
	player->render();

	SDL_RenderPresent(renderer);
}

void Game::clear() {
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	std::cout << "Game cleaned...\n";
}

bool Game::running() {
	return is_running;
}

bool Game::finished_game() {
	return ended;
}

bool Game::has_quited() {
	return quited;
}

void Game::rerun() {
	is_running = true;
}

bool Game::collision() {
	if (map->map[player->ypos / 32][player->xpos / 32] >= 2)
		return true;
	else
		return false;
}

void Game::change_map(){
	if (map->xpos == 0 && map->ypos == 1 && player->xpos / 32 == 3 && (player->ypos + 10) / 32 == 17) {
		map->ypos = 999;
		map->xpos = 999;
		player->xpos = 288;
		player->ypos = 18*32;
		
		map->change_map();
	}
	if (map->xpos == 999 && map->ypos == 999 && player->ypos == 608) {
		plot.left_house = true;

		player->xpos = (3 * 32);
		player->ypos = (18 * 32) - 8;
		map->xpos = 0;
		map->ypos = 1;

		map->change_map();
	}
	if (player->xpos == 608) {
		map->xpos++;
		map->change_map();
		player->xpos = 8;
	}
	else if (player->xpos == 4) {
		map->xpos--;
		map->change_map();
		player->xpos = 604;
	}
	else if (player->ypos == 4) {
		map->ypos--;
		map->change_map();
		player->ypos = 604;
	}
	else if (player->ypos == 608) {
		map->ypos++;
		map->change_map();
		player->ypos = 8;
	}
}

bool Game::met_poke() { 
	if (!ran) {
		for (auto& x : vec_poke) {
			if (player->xpos / (32 * size_multiply) == x.pos_X && player->ypos / (32 * size_multiply) == x.pos_Y && map->xpos == x.map_pos_X && map->ypos == x.map_pos_Y) {
				plot.fought_pokemon = true;
				return true;
			}
		}
		ran_turns = 10;
	}

	if (ran_turns <= 0)
		ran = false;

	return false;
}

bool Game::met_NPC() {
	for (auto& x : vec_npc) {
		if (player->xpos / (32 * size_multiply) == x.xpos && player->ypos / (32 * size_multiply) == x.ypos && map->xpos == x.map_xpos && map->ypos == x.map_ypos && x.is_action() && !x.visited) {
			x.visited = true;
			if (x.name == "Queen")
				plot.visited_queen = true;
			else if (x.name == "Prof")
				plot.visited_prof = true;
			else if (x.name == "Boss") {
				plot.fought_boss = true;
			}
			return true;
		}
	}

	return false;
}

Pokemon Game::get_poke_from_map() {
	for (auto x : vec_poke) {
		if (player->xpos / 32 == x.pos_X && player->ypos / 32 == x.pos_Y && map->xpos == x.map_pos_X && map->ypos == x.map_pos_Y)
			return x;
	}
}

NPC Game::get_NPC_from_map() {
	for (auto x : vec_npc) {
		if (player->xpos / (32 * size_multiply) == x.xpos && player->ypos / (32 * size_multiply) == x.ypos && map->xpos == x.map_xpos && map->ypos == x.map_ypos) {
			return x;
		}
	}
}

void Game::get_pokes_from_map(std::vector<Pokemon>& vec) {
	for (auto x : vec_poke) {
		if (player->xpos / 32 == x.pos_X && player->ypos / 32 == x.pos_Y && map->xpos == x.map_pos_X && map->ypos == x.map_pos_Y)
			vec.push_back(x);
	}
}

void Game::walka() {
	int lp = 1;
	bool win;
	xBattle battle;
	std::vector<Pokemon> vecp;

	get_pokes_from_map(vecp);

	std::cout << "do pokonona " << vecp.size() << "\n";

	for (auto x : vecp) {
		std::cout << "walka numer "<<lp<<"\n";
		win = battle.fight(player, x); 

		if (win)
			Game::delete_wild_poke();

		auto lam = std::remove_if(player->pokemons.begin(), player->pokemons.end(), [&](Pokemon& p1) {return p1.get_health_points() <= 0; });
		player->pokemons.erase(lam, player->pokemons.end());
		
		lp++;
	}

	if (win) {
		std::cout << "wygrana wojna\n";
		Game::delete_wild_poke();
	}


	std::cout << "mam: " << player->pokemons.size() << " pokemonow\n";

	if (battle.get_no_poke()) {
		is_running = false;
		std::cout << "Brak pokemonow\n";
	}

	ran = battle.get_run();
	if (ran)
		ran_turns = 10;
}

void Game::delete_wild_poke(){
	for (auto x = vec_poke.begin(); x != vec_poke.end(); x++) {
		if (player->xpos / 32 == x->pos_X && player->ypos / 32 == x->pos_Y && map->xpos == x->map_pos_X && map->ypos == x->map_pos_Y) {
			vec_poke.erase(x);
			return;
		}
	}
}

void Game::give_player_poke(const std::vector<Pokemon>& vec_poke) {
	player->pokemons.clear();

	for (const auto& x : vec_poke) {
		player->pokemons.push_back(x);
	}
}

void Game::game_end() {
	if (map->xpos == 999 && map->ypos == 999 && player->ypos <= 550 && plot.visited_prof) {
		std::cout << "koniec gry bo len\n";
		is_running = false;
		ended = true;
	}
	else if (map->xpos == 3 && map->ypos == 1 && player->xpos/32 == 18  && player->ypos/32 == 10 && plot.fought_boss) {
		std::cout << "koniec gry bo meta po wygranej\n";
		is_running = false;
		ended = true;
	}
	else if (map->xpos == 3 && map->ypos == 1 && player->xpos == 200 && player->ypos == 32) {
		std::cout << "koniec gry bo meta\n";
		is_running = false;
		ended = true;
	}

}

void Game::change_poke_order() {
	bool running = true;
	int pick1_id = -1, pick2_id = -1, pos = 0;
	std::vector<std::string> vec_name;
	std::vector<float> vec_pro;
	std::vector<SDL_Texture*> normal_vec_tex_name;
	std::vector<SDL_Texture*> picked_vec_tex_name;
	std::vector<SDL_Texture*> marked_vec_tex_name;
	SDL_Rect dest, src, bar_dest, bar_src;
	SDL_Texture* hp_bar = Tex_manager::load_texture("assets/HP_ba.png");

	for (auto poke : player->pokemons) {
		float temp_pro = float(poke.health_points) / float(poke.base_health_points);
		vec_pro.push_back(temp_pro);
		vec_name.push_back(poke.name);
	}

	for (auto poke : vec_name) {
		std::cout <<"nazwa "<< poke << "\n";
		if (poke == "Charmander") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/charmander_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/charmander_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/charmander_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
		else if (poke == "Bulbasaur") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/bulbasaur_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/bulbasaur_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/bulbasaur_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
		else if (poke == "Mew") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/mew_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/mew_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/mew_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
		else if (poke == "Pikachu") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/pikachu_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/pikachu_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/pikachu_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
		else if (poke == "Pidgey") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/pidgey_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/pidgey_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/pidgey_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
		else if (poke == "Squirtle") {
			SDL_Texture* temp_normal = Tex_manager::load_texture("assets/squirtle_name.png");
			SDL_Texture* temp_picked = Tex_manager::load_texture("assets/squirtle_name_picked.png");
			SDL_Texture* temp_marked = Tex_manager::load_texture("assets/squirtle_name_marked.png");
			normal_vec_tex_name.push_back(temp_normal);
			picked_vec_tex_name.push_back(temp_picked);
			marked_vec_tex_name.push_back(temp_marked);
		}
	}

	while(running){
		SDL_Event* eventt = new SDL_Event();
		SDL_PollEvent(eventt);
		
		switch (eventt->type) {
			case SDL_QUIT:
				is_running = false;
				break;
			case SDL_KEYDOWN:
				if (eventt->key.keysym.sym == SDLK_UP) {
					pos--;
					if (pos == -1)
						pos = 0;
					break;
				}
				else if (eventt->key.keysym.sym == SDLK_DOWN) {
					pos++;
					if (pos == vec_name.size())
						pos = vec_name.size() - 1;
					break;
				}
				else if (eventt->key.keysym.sym == SDLK_RETURN or eventt->key.keysym.sym == SDLK_KP_ENTER) {
					if(pick1_id == -1)
						pick1_id = pos;
					else {
						pick2_id = pos;
						running = false;
					}
				}
		}

		src.x = 0;
		src.y = 0;
		src.h = 64;
		src.w = 256;

		bar_src.x = 0;
		bar_src.y = 0;
		bar_src.w = 256;
		bar_src.h = 32;
		
		dest.h = 64;
		dest.w = 256;
		dest.x = 64;

		bar_dest.x = 321;
		bar_dest.h = 32;

		SDL_RenderClear(Game::renderer);
		
		for (int i = 0; i < vec_name.size(); i++) {
			dest.y = 128 + (i * 64);

			bar_dest.w = 256 * vec_pro[i];
			bar_dest.y = 144 + (i * 64);
			
			if (pick1_id == i) {
				Tex_manager::Draw(marked_vec_tex_name[i], src, dest);
			}
			else if (pos == i) {
				Tex_manager::Draw(picked_vec_tex_name[i], src, dest);
			}
			else {
				Tex_manager::Draw(normal_vec_tex_name[i], src, dest);
			}

			Tex_manager::Draw(hp_bar, bar_src, bar_dest);
		}

		SDL_RenderPresent(Game::renderer);
	}

	std::swap(player->pokemons[pick1_id], player->pokemons[pick2_id]);
}

void Game::render_manual() {
	TTF_Font* Sans = TTF_OpenFont("assets/arial.ttf", 20);

	std::string str1 = "-> zeby ruszyc sie w lewo";
	std::string str2 = "<- zeby ruszyc sie w prawo";
	std::string str3 = " ^ zeby ruszyc sie w gore";
	std::string str4 = "do zeby ruszyc sie w dol";
	std::string str5 = "\"p\" zeby zmienic kolejnosc pokemonow";
	std::string str6 = "\"enter\" zeby wybrac opcje";

	SDL_Color White = { 0, 0, 0 };
	SDL_Surface* surfaceMessage1 = NULL;
	SDL_Surface* surfaceMessage2 = NULL;
	SDL_Surface* surfaceMessage3 = NULL;
	SDL_Surface* surfaceMessage4 = NULL;
	SDL_Surface* surfaceMessage5 = NULL;
	SDL_Surface* surfaceMessage6 = NULL;
	surfaceMessage1 = TTF_RenderText_Solid(Sans, str1.c_str(), White);
	surfaceMessage2 = TTF_RenderText_Solid(Sans, str2.c_str(), White);
	surfaceMessage3 = TTF_RenderText_Solid(Sans, str3.c_str(), White);
	surfaceMessage4 = TTF_RenderText_Solid(Sans, str4.c_str(), White);
	surfaceMessage5 = TTF_RenderText_Solid(Sans, str5.c_str(), White);
	surfaceMessage6 = TTF_RenderText_Solid(Sans, str6.c_str(), White);

	SDL_Texture* Message1 = NULL;
	SDL_Texture* Message2 = NULL;
	SDL_Texture* Message3 = NULL;
	SDL_Texture* Message4 = NULL;
	SDL_Texture* Message5 = NULL;
	SDL_Texture* Message6 = NULL;
	Message1 = SDL_CreateTextureFromSurface(renderer, surfaceMessage1);
	Message2 = SDL_CreateTextureFromSurface(renderer, surfaceMessage2);
	Message3 = SDL_CreateTextureFromSurface(renderer, surfaceMessage3);
	Message4 = SDL_CreateTextureFromSurface(renderer, surfaceMessage4); 
	Message5 = SDL_CreateTextureFromSurface(renderer, surfaceMessage5); 
	Message6 = SDL_CreateTextureFromSurface(renderer, surfaceMessage6); 

	SDL_Rect Message_rect;
	Message_rect.x = 0;
	Message_rect.y = 0;
	Message_rect.w = 640;
	Message_rect.h = 64;
	
	SDL_RenderClear(renderer);

	Message_rect.x = 0;
	Message_rect.y = 0;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message1, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 64;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message2, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 128;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message3, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 192;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message4, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 256;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message5, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 320;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message6, NULL, &Message_rect);

	SDL_RenderPresent(renderer);

	bool flag = true;
	while (flag) {
		SDL_Event* event = new SDL_Event();
		SDL_PollEvent(event);

		switch (event->type) {
		case SDL_QUIT:
			is_running = false;
			flag = false;
			break;
		case SDL_KEYDOWN:
			if (event->key.keysym.sym == SDLK_LEFT) {

			}
			else if (event->key.keysym.sym == SDLK_RETURN or event->key.keysym.sym == SDLK_KP_ENTER) {
				flag = false;
			}
		default:
			break;
		}
		delete event;
	}
}

int Game::menu() {
	bool running = true, first = true;
	int posy = 0;

	int option = posy;

	SDL_Rect src, dest;

	src.x = 0;
	src.y = 0;
	src.h = 64;
	src.w = 256;

	dest.h = 64;
	dest.w = 256;
	dest.x = 192;

	SDL_RenderClear(renderer);

	dest.y = 320;
	if (option == 0)
		Tex_manager::Draw(picked_game, src, dest);
	else
		Tex_manager::Draw(game_tex, src, dest);

	dest.y += 64;
	if (option == 1)
		Tex_manager::Draw(picked_manual, src, dest);
	else
		Tex_manager::Draw(manual, src, dest);

	dest.y += 64;
	if (option == 2)
		Tex_manager::Draw(picked_credits, src, dest);
	else
		Tex_manager::Draw(credits, src, dest);

	dest.y += 64;
	if (option == 3)
		Tex_manager::Draw(picked_reset, src, dest);
	else
		Tex_manager::Draw(reset, src, dest);

	dest.y += 64;
	if (option == 4)
		Tex_manager::Draw(picked_quit, src, dest);
	else
		Tex_manager::Draw(quit, src, dest);




	SDL_RenderPresent(renderer);

	while (running) {
		SDL_Event* event_skill = new SDL_Event();
		SDL_PollEvent(event_skill);

		switch (event_skill->type) {
			case SDL_QUIT:
				is_running = false;
				break;
			case SDL_KEYDOWN:
				if (event_skill->key.keysym.sym == SDLK_UP) {
					posy--;
					if (posy == -1)
						posy = 0;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_DOWN) {
					posy++;
					if (posy == 5)
						posy = 4;
					break;
				}
				else if (event_skill->key.keysym.sym == SDLK_RETURN or event_skill->key.keysym.sym == SDLK_KP_ENTER) {
					running = false;
				}
		}

		option = posy;

		dest.y = 320;
		if (option == 0)
			Tex_manager::Draw(picked_game, src, dest);
		else
			Tex_manager::Draw(game_tex, src, dest);

		dest.y += 64;
		if (option == 1)
			Tex_manager::Draw(picked_manual, src, dest);
		else
			Tex_manager::Draw(manual, src, dest);

		dest.y += 64;
		if (option == 2)
			Tex_manager::Draw(picked_credits, src, dest);
		else
			Tex_manager::Draw(credits, src, dest);

		dest.y += 64;
		if (option == 3)
			Tex_manager::Draw(picked_reset, src, dest);
		else
			Tex_manager::Draw(reset, src, dest);
	
		dest.y += 64;
		if (option == 4)
			Tex_manager::Draw(picked_quit, src, dest);
		else
			Tex_manager::Draw(quit, src, dest);

		SDL_RenderPresent(renderer);

		delete event_skill;
	}

	return option;
}

void Game::talk_to() {
	int i = 0;
	std::string s1, s2;
	
	NPC npc = get_NPC_from_map();
	
	SDL_Color black = { 0, 0, 0 };
	SDL_Rect Message_rect1;
	
	TTF_Font* Sans = TTF_OpenFont("assets/arial.ttf", 15);
	SDL_Rect rect_nic, rect_nic1;
	rect_nic.x = 0;
	rect_nic.y = 640;
	rect_nic.w = 640;
	rect_nic.h = 150;

	rect_nic1.x = 0;
	rect_nic1.y = 0;
	rect_nic1.w = 1;
	rect_nic1.h = 1;

	for (int i = 0; i < npc.senteces.size()-1 ; i++) {
		s1 = npc.senteces[i];
		s2 = npc.senteces[i + 1];

		if (i + 3 <= npc.senteces.size())
			s2 += "...";
		
		surfaceMessage1 = TTF_RenderText_Solid(Sans, s1.c_str(), black);
		surfaceMessage2 = TTF_RenderText_Solid(Sans, s2.c_str(), black);

		Message1 = SDL_CreateTextureFromSurface(renderer, surfaceMessage1);
		Message2 = SDL_CreateTextureFromSurface(renderer, surfaceMessage2);

		Message_rect1.x = 0;
		Message_rect1.y = 640;
		Message_rect1.w = 640;
		Message_rect1.h = 64;
		SDL_RenderCopy(renderer, Message1, NULL, &Message_rect1);

		Message_rect1.x = 0;
		Message_rect1.y = 640 + 64;
		Message_rect1.w = 640;
		Message_rect1.h = 64;

		SDL_RenderCopy(renderer, Message2, NULL, &Message_rect1);
		SDL_RenderPresent(renderer);

		bool flag = true;
		while (flag) {
			SDL_Event* event = new SDL_Event();
			SDL_PollEvent(event);

			switch (event->type) {
			case SDL_QUIT:
				is_running = false;
				flag = false;
				break;
			case SDL_KEYDOWN:
				if (event->key.keysym.sym == SDLK_LEFT) {

				}
				else if (event->key.keysym.sym == SDLK_RETURN or event->key.keysym.sym == SDLK_KP_ENTER) {
					flag = false;
				}
			default:
				break;
			}
			delete event;
		}

		SDL_RenderCopy(Game::renderer, nic, &rect_nic1, &rect_nic);
		SDL_RenderPresent(Game::renderer);
	}

	if (npc.name == "Slave") {
		delete_npc("Slave");
	}
	else if (npc.name == "Boss") {
		delete_npc("Boss");
	}

	TTF_CloseFont(Sans);
}

void Game::delete_npc(const std::string& xname) {
	for (auto it = vec_npc.begin(); it != vec_npc.end(); it++) {
		if (it->name == xname) {
			vec_npc.erase(it);
			break;
		}
	}
}

void Game::display_end_scene() {
	std::vector<std::string> str;

	TTF_Font* Sans = TTF_OpenFont("assets/arial.ttf", 20);
	SDL_Color White = { 0, 0, 0 };
	SDL_Surface* surface;
	SDL_Texture* texture;

	SDL_Rect Message_rect;
	Message_rect.x = 0;
	Message_rect.y = 0;
	Message_rect.w = 640;
	Message_rect.h = 64;

	if (plot.visited_prof && !plot.fought_boss) {
		str.push_back("    No i nie zostales bohaterem,");
		str.push_back("a krolewna umarla w potwornych  ");
		str.push_back("meczarniach               |     ");
		str.push_back("                         <-|    ");
	}
	else if (plot.fought_boss) {
		str.push_back("    Brawo zostales bohaterem    ");
		str.push_back("uratowales ksiezniczke. Cale    ");
		str.push_back("krolestwo Ci dziekuje.    |     ");
		str.push_back("                         <-|    ");
	}
	//TODO

	SDL_RenderClear(Game::renderer);

	for (auto& x : str) {
		surface = TTF_RenderText_Solid(Sans, x.c_str(), White);
		texture = SDL_CreateTextureFromSurface(Game::renderer, surface);
		Message_rect.y += 64;
		SDL_RenderCopy(renderer, texture, NULL, &Message_rect);
	}
	
	SDL_RenderPresent(Game::renderer);

	bool flag = true;
	while (flag) {
		SDL_Event* event = new SDL_Event();
		SDL_PollEvent(event);

		switch (event->type) {
		case SDL_QUIT:
			is_running = false;
			flag = false;
			break;
		case SDL_KEYDOWN:
			if (event->key.keysym.sym == SDLK_LEFT) {

			}
			else if (event->key.keysym.sym == SDLK_RETURN or event->key.keysym.sym == SDLK_KP_ENTER) {
				flag = false;
			}
		default:
			break;
		}
		delete event;
	}
}

bool Game::has_pressed_esc() {
	return escape_pressed;
}

void Game::neg_esc() {
	escape_pressed = false;
}

void Game::neg_finished() {
	if (ended)
		ended = false;
	else
		ended = true;
}

void Game::render_creadits() {
	TTF_Font* Sans = TTF_OpenFont("assets/arial.ttf", 20);

	std::string str1 = "Nad gra pracowali         ";
	std::string str2 = "-> Piotr Kwiatkowski      ";
	std::string str3 = "piotr.d.kwiatkowski@student.put.poznan.pl";
	std::string str4 = "-> Julia Omachel          ";

	SDL_Color White = { 0, 0, 0 };
	SDL_Surface* surfaceMessage1 = NULL;
	SDL_Surface* surfaceMessage2 = NULL;
	SDL_Surface* surfaceMessage3 = NULL;
	SDL_Surface* surfaceMessage4 = NULL;
	surfaceMessage1 = TTF_RenderText_Solid(Sans, str1.c_str(), White);
	surfaceMessage2 = TTF_RenderText_Solid(Sans, str2.c_str(), White);
	surfaceMessage3 = TTF_RenderText_Solid(Sans, str3.c_str(), White);
	surfaceMessage4 = TTF_RenderText_Solid(Sans, str4.c_str(), White);

	SDL_Texture* Message1 = NULL;
	SDL_Texture* Message2 = NULL;
	SDL_Texture* Message3 = NULL;
	SDL_Texture* Message4 = NULL;
	Message1 = SDL_CreateTextureFromSurface(renderer, surfaceMessage1);
	Message2 = SDL_CreateTextureFromSurface(renderer, surfaceMessage2);
	Message3 = SDL_CreateTextureFromSurface(renderer, surfaceMessage3);
	Message4 = SDL_CreateTextureFromSurface(renderer, surfaceMessage4);

	SDL_Rect Message_rect;
	Message_rect.x = 0;
	Message_rect.y = 0;
	Message_rect.w = 640;
	Message_rect.h = 64;

	SDL_RenderClear(renderer);

	Message_rect.x = 0;
	Message_rect.y = 0;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message1, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 64;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message2, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 128;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message3, NULL, &Message_rect);

	Message_rect.x = 0;
	Message_rect.y = 192;
	Message_rect.w = 640;
	Message_rect.h = 64;
	SDL_RenderCopy(renderer, Message4, NULL, &Message_rect);

	SDL_RenderPresent(renderer);

	bool flag = true;
	while (flag) {
		SDL_Event* event = new SDL_Event();
		SDL_PollEvent(event);

		switch (event->type) {
		case SDL_QUIT:
			is_running = false;
			flag = false;
			break;
		case SDL_KEYDOWN:
			if (event->key.keysym.sym == SDLK_LEFT) {

			}
			else if (event->key.keysym.sym == SDLK_RETURN or event->key.keysym.sym == SDLK_KP_ENTER) {
				flag = false;
			}
		default:
			break;
		}
		delete event;
	}
}